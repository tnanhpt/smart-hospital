import { Component, OnInit, HostBinding, HostListener } from '@angular/core';
import { moveIn, fallIn } from 'src/app/router.animation';
import { AuthService } from 'src/app/module/core/auth/service/auth.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  constructor(public auth: AuthService) { }

  ngOnInit() {
  }

}
