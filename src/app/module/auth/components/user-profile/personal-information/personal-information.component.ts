import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/module/core/auth/service/auth.service';
import { FormGroup, FormControl } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { finalize, map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { moveIn, fadeIn } from 'src/app/router.animation';

// const datePipe = new DatePipe('en-US');
@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.scss']
})
export class PersonalInformationComponent implements OnInit {
  // minDate = new Date(2000, 0, 1);
  maxDate = new Date();
  isLoading = false;
  updateInfoForm = new FormGroup({
    uid: new FormControl(''),
    displayName: new FormControl(''),
    phoneNumber: new FormControl(''),
    email: new FormControl(''),
    birthday: new FormControl(''),
    address: new FormControl(''),
    gender: new FormControl(''),
    photoURL: new FormControl('')
  })
  constructor(public auth: AuthService,
    private dateAdapter: DateAdapter<Date>,
    private storage: AngularFireStorage,
    private toastr: ToastrService
  ) {
    this.dateAdapter.setLocale('vi');
  }


  ngOnInit() {
    this.auth.user.subscribe(user => {
      if (user.birthday !== null) {
        const day = moment.unix(user.birthday.seconds);
        const dayD = day.format();
        const birthday = moment(dayD).format("YYYY-MM-DD");
        this.updateInfoForm.setValue({
          uid: user.uid,
          displayName: user.displayName,
          address: user.address,
          phoneNumber: user.phoneNumber,
          email: user.email,
          birthday: birthday,
          gender: user.gender,
          photoURL: user.photoURL
        })
      } else {
        this.updateInfoForm.setValue({
          uid: user.uid,
          displayName: user.displayName,
          address: user.address,
          phoneNumber: user.phoneNumber,
          email: user.email,
          birthday: user.birthday,
          gender: user.gender,
          photoURL: user.photoURL
        })
      }
    })

  }
  updateInfo() {
    this.auth.updateUserDataByForm(this.updateInfoForm.value)
    this.toastr.success('Cập nhật thành công');
  }
  uploadPercent: Observable<number>;
  downloadURL: Observable<string>;
  uploadFile(event) {
    this.isLoading = true;
    const file = event.target.files[0];
    const filePath = 'name-your-file-path-here';
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe(
      finalize(() => {
        this.downloadURL = fileRef.getDownloadURL();
        this.isLoading = false;
        this.downloadURL.subscribe(res => {
          this.updateInfoForm.patchValue({
            photoURL: res
          });

        })
      })
    )
      .subscribe()

  }
  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes
  }

}
