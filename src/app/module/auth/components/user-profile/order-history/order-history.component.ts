import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/module/core/auth/service/auth.service';
import { OrderService } from 'src/app/module/share/service/order/order.service';
import { OrderIdModel, OrderModel } from 'src/app/module/core/auth/model/core.model';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { moveIn, fallIn, moveInLeft, fadeIn } from 'src/app/router.animation';
import * as moment from 'moment';
import { map } from 'rxjs/operators';
import { MatPaginator, MatTableDataSource } from '@angular/material';
@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent implements OnInit {
  isLoading = false;
  // order: OrderModel[];
  order: any;
  history: any;
  data: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  cols = ['Họ tên', 'Số điện thoại', 'Địa chỉ', 'Ngày khám', 'Khoa'];
  columnsToDisplay = ['displayName', 'phoneNumber', 'address', 'date', 'department','status'];
  constructor(public auth: AuthService, public os: OrderService) { }

  ngOnInit() {
    this.isLoading = true;
    this.auth.user.subscribe(user => {
      this.isLoading = false;
      const uid = user.uid;
      this.os.getOrderByUID(uid).subscribe(res => {
        if (res) {
          this.history = res;
          // console.log(res);
          console.log(JSON.stringify(res));
          // const data = JSON.parse(res);
          // this.order = this.data;
          this.order = new MatTableDataSource(res);
          this.order.paginator = this.paginator;
        }

      })
    })
  }
  applyFilter(filterValue: string) {
    this.order.filter = filterValue.trim().toLowerCase();
  }
}
