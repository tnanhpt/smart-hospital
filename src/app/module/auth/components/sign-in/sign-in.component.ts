import { Component, OnInit, OnChanges, SimpleChange, AfterViewInit, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { moveIn } from 'src/app/router.animation';
import { AuthService } from 'src/app/module/core/auth/service/auth.service';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { debounceTime, take, map } from 'rxjs/operators';
import * as firebase from 'firebase';
import { StatusUser, UserModel } from 'src/app/module/core/auth/model/core.model';
import { WindowService } from 'src/app/module/share/service/window/window.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  animations: [moveIn()],
  host: { '[@moveIn]': '' }
})
export class SignInComponent implements OnInit, OnChanges, AfterViewInit {
  loginWithEmailForm = new FormGroup({
    email: new FormControl('',
      [
        Validators.required,
        Validators.email,
        Validators.minLength(5),
        Validators.maxLength(30)
      ]),
    password: new FormControl('',
      [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20),

      ])
  });
  signUpWithEmailForm = new FormGroup({
    email: new FormControl('', [

      Validators.required,
      Validators.email,
      Validators.minLength(5),
      Validators.maxLength(30)
    ]),
    password: new FormControl('',
      [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20),

      ]),
    displayName: new FormControl('')
  })
  hide = true;
  isForgot = false;
  emailUser: string;
  isLoading = false;
  user: UserModel;
  phoneNum: string;
  windowRef: any;
  verificationCode: string;
  userPhone: string;
  isCode = false;
  constructor(public auth: AuthService,
    public routers: Router,
    public afs: AngularFirestore,
    private win: WindowService,
    private router: Router,
    private toastr: ToastrService,
  ) { this.isCode = false; }

  ngOnInit() {
    this.windowRef = this.win.windowRef;
    this.windowRef.confirmationResult = false;
    if (this.auth.user) {
      this.auth.user.subscribe(res => {
        if (res !== null) {
          if (res.emailVerified || res.phoneNumber !== null) {
            this.user = res;
            this.routers.navigate(['/app/dat-lich']);
          }

        }

      })

    }
  }
  ngAfterViewInit() {
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');

    this.windowRef.recaptchaVerifier.render();
  }

  ngOnChanges(changes: SimpleChanges): void {


  }


  loginWithEmail() {
    // this.isLoading = true;
    this.auth.emailLogin(this.loginWithEmailForm.value);
  }

  signUp() {
    // console.log(this.signUpWithEmailForm.value);

    // this.isLoading = true;
    return this.auth.emailSignUp(this.signUpWithEmailForm.value);
  }
  get email() {

    return this.signUpWithEmailForm.get('email')
  }

  get username() {
    return this.signUpWithEmailForm.get('username')
  }
  resetPassword(email: string) {
    var auth = firebase.auth();
    return auth.sendPasswordResetEmail(email)
      .then(() => console.log("email sent"))
      .catch((error) => console.log(error))
  }
  //Login with Phone

  sendLoginCode() {

    const num = '+84' + this.userPhone;
    console.log(num);

    const appVerifier = this.windowRef.recaptchaVerifier;
    firebase.auth().signInWithPhoneNumber(num, appVerifier)
      .then(result => {
        this.windowRef.confirmationResult = result;
      })
      .catch(error => {
        this.toastr.error(error.message, 'Lỗi!')
      });

  }

  verifyLoginCode() {
    this.isLoading = true;
    this.windowRef.confirmationResult
      .confirm(this.verificationCode)
      .then(result => {
        console.log(result);
        if (result.additionalUserInfo.isNewUser) {
          this.isLoading = false;
          this.auth.updateUserData(result.user)
          this.router.navigate(['/app/dat-lich']);
        } else {
          this.router.navigate(['/app/dat-lich']);
        }


      })
      .catch(error => {
        this.isLoading = false;
        this.toastr.error(error.message, 'Lỗi!')
        // console.log(error)
      });
  }
}

// export class CustomValidator {
//   static email(afs: AngularFirestore) {
//     return (control: AbstractControl) => {

//       const email = control.value.toLowerCase();

//       return afs.collection('users', ref => ref.where('email', '==', email))

//         .valueChanges().pipe(
//           debounceTime(500),
//           take(1),
//           map(arr => arr.length ? { emailAvailable: false } : null),
//         )
//     }
//   }
// }

