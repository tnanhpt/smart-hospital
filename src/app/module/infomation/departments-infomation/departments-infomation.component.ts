import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../core/auth/service/auth.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { DepartmentsService } from '../../share/service/deparments/departments.service';
import { DepartmentIDModel, OrderModel, OrderIdModel, StatusOrder } from '../../core/auth/model/core.model';
import { OrderService } from '../../share/service/order/order.service';
import { AngularFirestoreDocument, AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
@Component({
  selector: 'app-departments-infomation',
  templateUrl: './departments-infomation.component.html',
  styleUrls: ['./departments-infomation.component.scss']
})
export class DepartmentsInfomationComponent implements OnInit {
  departmentUser: DepartmentIDModel;
  orderCurrent: any;
  orderSeconds: any;
  statusOrder = StatusOrder;
  orderToday: any;
  orderDoc: AngularFirestoreDocument<OrderModel>;
  orderCol: AngularFirestoreCollection<OrderModel>;
  dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['displayName', 'address', 'birthday', 'gender', 'orderNumber', 'healthStatus', 'phoneNumber', 'status'];
  constructor(
    private afs: AngularFirestore,
    public auth: AuthService,
    public dialog: MatDialog,
    private toastr: ToastrService,
    public depS: DepartmentsService,
    public os: OrderService
  ) { moment.locale('vi') }

  ngOnInit() {
    this.auth.user.subscribe(user => {
      if (user) {
        this.depS.getDepartments().subscribe(dep => {
          if (dep) {
            // console.log(dep);
            try {
              for (const d of dep) {
                for (const doc of d.data.doctors) {
                  if (doc.uid === user.uid) {
                    this.departmentUser = d;
                    console.log(d);
                    
                    break;
                  }
                }
              }
              this.getOrderbyDepartment(this.departmentUser.data.name, this.departmentUser.data.orderNumberCurrent)
            } catch (error) {
              console.log(error);
            }
          }
        });
      }
    })
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getOrderbyDepartment(name, orderNumber) {
    orderNumber = parseInt(orderNumber);

    if (this.departmentUser) {
      // this.afs.collection('orders', ref => ref.where('department', '==', name)).valueChanges().subscribe(res => {
      //   t

      // })
      const today = moment().format('L')

      const orderCol = this.afs.collection('orders', ref =>
        ref.orderBy('dateOrder', 'asc').where('department', '==', name));
      orderCol.snapshotChanges().pipe(
        map(actions => {
          return actions.map(a => {
            const dataBefore = a.payload.doc.data() as OrderModel;
            const id = a.payload.doc.id;
            let data: OrderModel;
            if (dataBefore.date !== null && dataBefore.dateOrder !== null) {
              data = {
                ...dataBefore,
                birthday: moment.unix(dataBefore.birthday.seconds),
                date: (moment.unix(dataBefore.date.seconds)).format('L'),
                dateOrder: moment.unix(dataBefore.dateOrder.seconds),
              }
              return { id, data };
            } else {
              return { id, dataBefore };
            }
          });
        })).subscribe(res => {
          if (res) {
            this.orderToday = res;
            this.dataSource = new MatTableDataSource(res);
            this.dataSource.paginator = this.paginator;
            this.orderCurrent = res.filter(r => (r.data.date == today) && (r.data.status === StatusOrder.confirmed));
            // console.log(res);

          }
        });
    }
  }
  updateOrder(type: StatusOrder, id) {
    this.orderDoc = this.afs.doc('orders/' + id);
    this.orderDoc.update({ status: type }).then(() => this.toastr.success('Sửa thành công!')
    ).catch(error => this.toastr.success('Lỗi! Không thể thực hiện'))
  }
}
