
export interface OrderIdModel {
  id: string;
  data: OrderModel;
}
export interface OrderModel {
  id?: string;
  date?: any;
  dateOrder?: any;
  orderNumber?: number;
  // detail?: string;
  healthStatus?: string;
  department?: string;
  uid?: string;
  displayName?: string;
  email?: string,
  address?: string;
  birthday?: any;
  gender?: string;
  phoneNumber?: string;
  status?: StatusOrder;
  photoURL?: any;
}
export enum StatusUser {
  confirmed = 'Đã xác nhận',
  unconfirmed = 'Chưa xác nhận'
}
export enum StatusOrder {
  pending = 'Chờ duyệt',
  confirmed = 'Đã xác nhận',
  completed = 'Đã khám',
  canceled = 'Đã hủy khám',
  ignore = 'Qua lượt'

}
export interface UserModel {
  uid?: string;
  password?: string;
  email?: string;
  displayName?: string;
  phoneNumber?: number;
  gender?: string;
  birthday?: any;
  // age?: number;
  address?: string;
  type?: string;
  photoURL?: any;
  roles?: RolesModel;
  date?: any;
  emailVerified? : any;
}
export interface DepartmentIDModel {
  id?: string;
  data?: DepartmentModel;
}
export interface DepartmentModel {
  id?: string;
  name?: string;
  orderNumberCurrent?: number;
  doctors?: any[];
}
export enum RolesModel {
  admin = 'Admin',
  doctor = 'Bác sĩ',
  member = 'Thành viên'
}
