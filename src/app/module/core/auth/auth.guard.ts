import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take, map, tap } from 'rxjs/operators';
import { AuthService } from './service/auth.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthService,
    private router: Router,
    private toastr: ToastrService
  ) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.auth.user.pipe(
      take(1),
      map(user => {
        // return !!user;
        console.log(user);
        if (user) {
          if (user.emailVerified || user.phoneNumber !== null) {
            return !!user;
          } else {
            this.toastr.error('Vui lòng xác minh Email trước khi đăng nhập')
          }
        }

        else {
          this.router.navigate(['/sign-in']);
          this.toastr.error('Vui lòng đăng nhập')
        }
      })
    );
  }
}
