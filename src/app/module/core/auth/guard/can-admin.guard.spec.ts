import { TestBed, async, inject } from '@angular/core/testing';

import { CanAdminGuard } from './can-admin.guard';

describe('CanAdminGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanAdminGuard]
    });
  });

  it('should ...', inject([CanAdminGuard], (guard: CanAdminGuard) => {
    expect(guard).toBeTruthy();
  }));
});
