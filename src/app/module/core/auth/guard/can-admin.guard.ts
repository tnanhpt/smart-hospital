import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take, map, tap } from 'rxjs/operators';
import { AuthService } from '../service/auth.service';
import { RolesModel } from '../model/core.model';

@Injectable({
  providedIn: 'root'
})
export class CanAdminGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.auth.user.pipe(
      take(1),
      map(user => user.roles === RolesModel.admin),
      tap(loggedIn => {
        if (!loggedIn) {
          console.log('Không được phép, yêu cầu admin');
          this.router.navigate(['/tai-khoan']);
        }
      }));
  }
}
