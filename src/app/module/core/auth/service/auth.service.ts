import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { Observable, of } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { switchMap } from 'rxjs/operators';
import { UserModel, RolesModel, StatusUser } from '../model/core.model';
import { ToastrService } from 'ngx-toastr';
import { WindowService } from 'src/app/module/share/service/window/window.service';
@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit {
  user: Observable<UserModel>;
  isLoading = false;
  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    private toastr: ToastrService,
   
  ) {
    this.isLoading = false;
    this.user = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<UserModel>(`users/${user.uid}`).valueChanges();
        } else { return of(null); }
      })
    );
  }
  ngOnInit() {
   
  }
  // Sinup with Email
  emailSignUp(userReg: UserModel) {
    this.isLoading = true;
    return this.afAuth.auth.createUserWithEmailAndPassword(userReg.email, userReg.password)
      .then(res => {
        if (res) {
          console.log(res);

          const infoUser: UserModel = {
            ...userReg,
            uid: res.user.uid,
            emailVerified: res.user.emailVerified
          };
          this.isLoading = false;
          this.updateUserData(infoUser);

          let user: any = this.afAuth.auth.currentUser;
          user.sendEmailVerification().then(
            () => {
              this.isLoading = false;
              this.toastr.success('Vui lòng kiểm tra Email để xác thực tài khoản', 'Đăng ký thành công');
            }).catch(
              (e) => {
                this.isLoading = false;
                this.toastr.error(e.message)
                // console.log(e.message)
              })
        }
      }).catch(error => {
        this.isLoading = false;
        this.toastr.error(error.message)
      })
  }
  updateUserData(user) {
    this.isLoading = true;
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    let data: UserModel;
    // console.log(user);
    data = {
      ...data,
      uid: user.uid || null,
      displayName: user.displayName || 'Người dùng',
      roles: user.roles || RolesModel.member,
      email: user.email || null,
      photoURL: user.photoURL || '/assets/images/core/default-user-avt.jpg',
      phoneNumber: user.phoneNumber || null,
      address: user.address || null,
      gender: user.gender || null,
      birthday: user.birthday || null,
      date: this.getTimeServer(),
      emailVerified: user.emailVerified || false
    };
    return userRef.set(data, { merge: true });
}
  addUser(user: UserModel) {
    return this.afAuth.auth.createUserAndRetrieveDataWithEmailAndPassword(user.email, user.password)
      .then(createUser => {
        const infoUser: UserModel = {
          ...user,
          uid: createUser.user.uid,
        };
        this.updateUserData(infoUser);
        this.isLoading = true;
        // this.router.navigate(['/app/dat-lich']);
      }).catch(error => {
        this.isLoading = false;
        this.toastr.error(error.message)
      });
  }
  // Login with Email, Password
  emailLogin(data: UserModel) {
    this.isLoading = true;
    return this.afAuth.auth.signInWithEmailAndPassword(data.email.trim(), data.password.trim()).then(res => {
      if (res.user.emailVerified) {
        const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${res.user.uid}`);
        let data: UserModel;
        data = {
          ...data,
          emailVerified: res.user.emailVerified
        }
        userRef.update(data);
        this.isLoading = false;
        this.router.navigate(['/app/dat-lich']);
        this.toastr.success('Đăng nhập thành công');
      }
      else {
        this.isLoading = false;
        this.toastr.error('Vui lòng xác thực Email trước khi đăng nhập', 'Lỗi!');
      }

    }).catch(error => {
      this.isLoading = false;
      this.toastr.error(error.message)
    });
  }
  forgotPassword(email) {
    this.isLoading = true;
    return this.afAuth.auth.sendPasswordResetEmail(email.trim()).then(() => {
      this.toastr.success('Vui lòng kiểm tra email để lấy lại mật khẩu', 'Thành công!');
      this.isLoading = false;
    }).catch(e => {
      this.isLoading = false;
      this.toastr.error(e.message)
    });
  }
  getDoctors() {
    this.isLoading = true;
    return this.afs.collection<UserModel>('users', ref => ref.where('roles', '==', RolesModel.doctor)).valueChanges();
  }
  // Login with Google account
  googleLogin() {
    // this.isLoading = true;
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }
  googleRegistor() {
    // this.isLoading = true;
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }
  // Login with Facebook account
  facebookLogin() {
    // this.isLoading = true;
    const provider = new firebase.auth.FacebookAuthProvider();
    return this.oAuthLogin(provider);
  }
  // Handling after login Social
  oAuthLogin(provider) {
    // this.isLoading = true;
    return this.afAuth.auth.signInWithPopup(provider).then(credential => {
      // console.log(credential);
      if (credential.additionalUserInfo.isNewUser) {
        this.updateUserData(credential.user);


        this.router.navigate(['/app/dat-lich']);
      }
      else {
        this.router.navigate(['/app/dat-lich']);
      }
    }).catch(error => {
      this.isLoading = false;
      this.toastr.error(error.message)
    });
  }

  // Update data
  updateUserDataByForm(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    let data: UserModel;
    data = {
      ...data,
      uid: user.uid || null,
      displayName: user.displayName || null,
      email: user.email || null,
      photoURL: user.photoURL || '/assets/images/core/default-user-avt.jpg',
      phoneNumber: user.phoneNumber || null,
      address: user.address || null,
      gender: user.gender || null,
      birthday: user.birthday || null,
      date: this.getTimeServer()
    };
    return userRef.set(data, { merge: true });
  }

  // Log out
  signOut() {
    this.isLoading = true;
    this.afAuth.auth.signOut().then(() => {
      this.isLoading = false;
      this.router.navigate(['/sign-in']);
      this.toastr.success('Đăng xuất thành công!')
    }).catch(error => {
      this.isLoading = false;
      this.toastr.error(error.message)
    });;
  }

  canRead(user: UserModel): boolean {
    const allowed = ['admin', 'doctor', 'member']
    return this.checkAuthorization(user, allowed)
  }

  canEdit(user: UserModel): boolean {
    const allowed = ['admin', 'doctor']
    return this.checkAuthorization(user, allowed)
  }

  canDelete(user: UserModel): boolean {
    const allowed = ['admin']
    return this.checkAuthorization(user, allowed)
  }
  checkAuthorization(user: UserModel, allowedRoles: string[]): boolean {
    if (!user) return false
    for (const role of allowedRoles) {
      if (user.roles[role]) {
        return true
      }
    }
    return false
  }
  getTimeServer() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }


 
}
