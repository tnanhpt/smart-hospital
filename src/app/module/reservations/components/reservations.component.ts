import { Component, OnInit, OnChanges } from '@angular/core';
import { AngularFirestoreDocument, AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { DepartmentModel, DepartmentIDModel, OrderModel, UserModel, OrderIdModel, StatusOrder } from '../../core/auth/model/core.model';
import { map, take, find, filter } from 'rxjs/operators';
import { DepartmentsService } from '../../share/service/deparments/departments.service';
import { FormGroup, FormControl } from '@angular/forms';
import { OrderService } from '../../share/service/order/order.service';
import { AuthService } from '../../core/auth/service/auth.service';
import * as firebase from 'firebase/app';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { DateAdapter } from '@angular/material/core';
@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.scss']
})
export class ReservationsComponent implements OnInit, OnChanges {
  derpartments$: Observable<DepartmentIDModel[]>;
  order$: Observable<OrderModel[]>;
  authCurrent: UserModel;
  lastOrderNameDepartment: string;
  minDate = new Date();
  maxDate = new Date();
  isRever = true;
  isConfirm = true;
  isLoading=  false;
  afterOrder: OrderModel;
  // departmentSingle: Observable<any>;
  checkoutForm = new FormGroup({
    displayName: new FormControl(''),
    // age: new FormControl(''),
    address: new FormControl(''),
    phoneNumber: new FormControl(''),
    email: new FormControl(''),
    birthday: new FormControl(''),
    gender: new FormControl(''),
    healthStatus: new FormControl(''),
    date: new FormControl(''),
    departmentName: new FormControl('')
  });
  constructor(
    private afs: AngularFirestore,
    public department: DepartmentsService,
    private os: OrderService,
    public auth: AuthService,
    private toastr: ToastrService,
    private dateAdapter: DateAdapter<Date>
  ) {
    this.dateAdapter.setLocale('vi');
  }
  ngOnInit() {
    this.auth.user.subscribe(user => {
      this.authCurrent = user;
      let birthday: any;
      if (user.birthday !== null) {
        // birthday = moment.unix(user.birthday.seconds).format("YYYY-MM-DD");

        // birthday = f.format("YYYY-MM-DD");
        // console.log(dayD);
        // const birthday = moment(dayD).format("YYYY-MM-DD");

      } else {
        birthday = user.birthday;
      }
      console.log(user);
      
      this.checkoutForm.setValue({
        displayName: user.displayName,
        address: user.address,
        phoneNumber: user.phoneNumber,
        email: user.email,
        birthday: user.birthday,
        gender: user.gender,
        healthStatus: (''),
        date: (''),
        departmentName: ('')
      });
    })
    this.derpartments$ = this.department.getDepartments();
    this.order$ = this.os.getOrder();
  }
  ngOnChanges() {
  }
  checkOut() {
    this.isLoading = true;
    const ordersCollection: AngularFirestoreCollection<OrderModel> = this.afs.collection<any>('orders', ref =>
      ref.where('date', '==', this.checkoutForm.value.date)
    );
    ordersCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as OrderModel;
          const id = a.payload.doc.id;
          return { id, data };
        });
      })).pipe(take(1)).subscribe(order => {
        if (order) {
          const orderDepart = order.filter(a => a.data.department = this.checkoutForm.value.departmentName);


          if (orderDepart) {
            console.log(orderDepart);
            const numberOrder = orderDepart.length + 1
            this.addOrder(this.checkoutForm.value, this.authCurrent.uid, this.authCurrent.photoURL, numberOrder);
          }
        }


      })
    // this.toastr.success('Đặt lịch thành công');
    this.isRever = false;
  }
  updateDepartmentData(id, orderNumber) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`departments/${id}`);
    let data: DepartmentModel;
    data = {
      ...data,
      orderNumberCurrent: orderNumber
    };
    return userRef.set(data, { merge: true });
  }
  delOrders(id) {
    this.afs.doc('orders/' + id).delete();
  }
  addOrder(data, uid, avt, orderNumberLast) {
    this.afs.collection<OrderModel>('orders').add(
      {
        'dateOrder': this.getTimeServer(),
        'date': data.date,
        'orderNumber': orderNumberLast,
        'healthStatus': data.healthStatus,
        'department': data.departmentName,
        'displayName': data.displayName,
        'uid': uid || null,
        'email': data.email,
        'birthday': data.birthday,
        'phoneNumber': data.phoneNumber,
        'address': data.address,
        'gender': data.gender,
        'photoURL': avt,
        'status': StatusOrder.pending
      }
    )
      .then((success) => {
        const successDoc: AngularFirestoreDocument<OrderModel> = this.afs.doc('orders/' + success.id);
        successDoc.valueChanges().subscribe(res => {
          if (res) {
            this.toastr.success('Đặt lịch thành công!')
            const data: OrderModel = {
              ...res,
              date: moment.unix(res.date.seconds)
            };
            this.afterOrder = data;
            this.isConfirm = false;
            console.log(res.date);
            this.isLoading = false;
          }
        });
      }
      )
      .catch(error => {
        this.isLoading = false;
        this.toastr.error(error.message)
      });
  }
  getTimeServer() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }
  submit() {
    this.isRever = false;
  }
  printTicket() {
    window.print();
  }
}
