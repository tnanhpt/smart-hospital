import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminPageComponent } from '../admin-page-container/admin-page.component';
import { AuthGuard } from 'src/app/module/core/auth/auth.guard';
import { DepartmentsComponent } from '../components/departments/departments.component';
import { OrdersManagerComponent } from '../components/orders-manager/orders-manager.component';
import { UserManagerComponent } from '../components/user-manager/user-manager.component';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { CanAdminGuard } from '../../core/auth/guard/can-admin.guard';

const routes: Routes = [
  {
    path: 'admin',
    component: AdminPageComponent,
    canActivate: [CanAdminGuard],
    children: [
      { path: '', canActivate: [CanAdminGuard], component: DashboardComponent, },
      { path: 'khoa',  canActivate: [CanAdminGuard],component: DepartmentsComponent },
      { path: 'lich-kham',  canActivate: [CanAdminGuard],component: OrdersManagerComponent },
      { path: 'nguoi-dung',  canActivate: [CanAdminGuard],component: UserManagerComponent },
      { path: 'thong-ke', canActivate: [CanAdminGuard], component: DashboardComponent }
    ]
  }
]
@NgModule({
  imports: [
  RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRouterModule { }
