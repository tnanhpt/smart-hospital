import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AuthService } from 'src/app/module/core/auth/service/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { OrderService } from 'src/app/module/share/service/order/order.service';
import { Observable } from 'rxjs';
import { OrderModel, OrderIdModel, StatusOrder } from 'src/app/module/core/auth/model/core.model';
import * as moment from 'moment';
import { map, filter, find } from 'rxjs/operators';
import { fadeIn } from 'src/app/router.animation';
@Component({
  selector: 'app-order-new',
  templateUrl: './order-new.component.html',
  styleUrls: ['./order-new.component.scss']
})
export class OrderNewComponent implements OnInit {

  displayedColumns: string[] = ['displayName', 'department', 'status', 'dateOrder'];

  dataSource;
  orders$: Observable<OrderIdModel>;
  orders: OrderModel[];
  ordersId: any;
  orderSingle: OrderModel[];
  orderCol: AngularFirestoreCollection<OrderModel>;
  orderDoc: AngularFirestoreDocument<OrderModel>;
  @Output() totalOrders = new EventEmitter<number>();
  @Output() newOrders = new EventEmitter<number>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(
    private afs: AngularFirestore,
    public auth: AuthService,
    public dialog: MatDialog,
    private toastr: ToastrService,
    private os: OrderService,
  ) { }

  ngOnInit() {
    this.orderCol = this.afs.collection('orders', ref =>
      ref.orderBy('dateOrder', 'desc'));
    this.orderCol.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const dataBefore = a.payload.doc.data() as OrderModel;
          moment.locale('vi')
          const id = a.payload.doc.id;
          let data: OrderModel;
          if (dataBefore.birthday !== null && dataBefore.date !== null && dataBefore.dateOrder !== null) {
            data = {
              ...dataBefore,
              birthday: moment.unix(dataBefore.birthday.seconds),
              date: moment.unix(dataBefore.date.seconds),
              dateOrder: moment.unix(dataBefore.dateOrder.seconds).fromNow(),
            }
            return { id, data };
          } else {
            return { id, dataBefore };
          }
        });
      })).subscribe(res => {
        if (res) {
          this.totalOrders.emit(res.length);
          this.dataSource = new MatTableDataSource(res);
          this.dataSource.paginator = this.paginator;
        }
      });
    const ordersCollection: AngularFirestoreCollection<OrderModel> = this.afs.collection<any>('orders', ref =>
      ref.where('status', '==', StatusOrder.pending)
    );
    ordersCollection.valueChanges().subscribe(res => this.newOrders.emit(res.length));
  }


}
