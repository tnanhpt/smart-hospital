import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AuthService } from 'src/app/module/core/auth/service/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { UserModel } from 'src/app/module/core/auth/model/core.model';
import * as moment from 'moment';
import { map } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { fadeIn } from 'src/app/router.animation';
@Component({
  selector: 'app-user-new',
  templateUrl: './user-new.component.html',
  styleUrls: ['./user-new.component.scss']
})
export class UserNewComponent implements OnInit {
  users$: Observable<UserModel>;
  users: UserModel[];
  userSingle: UserModel[];
  usersCol: AngularFirestoreCollection<UserModel>;
  usersDoc: AngularFirestoreDocument<UserModel>;
  dataSource;
  @Output() totalUser = new EventEmitter<number>();
  displayedColumns: string[] = ['photoURL', 'displayName', 'address', 'date'];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private afs: AngularFirestore,
    public auth: AuthService,
    public dialog: MatDialog,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    moment.locale('vi')
    this.usersCol = this.afs.collection('users', ref => ref.orderBy('date','desc'));
    this.usersCol.valueChanges().pipe(map((data => data.map(a => {
      let dataS: UserModel;
      console.log(a.date);

      dataS = {
        ...a,
        date: moment.unix(a.date.seconds).fromNow(),
      }
      return dataS;

    }
    )))).subscribe(res => {
      if (res) {
        this.totalUser.emit(res.length);
        this.dataSource = new MatTableDataSource<UserModel>(res);
        this.dataSource.paginator = this.paginator;
      }
    })
  }

}
