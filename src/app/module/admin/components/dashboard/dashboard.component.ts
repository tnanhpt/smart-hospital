import { Component, OnInit } from '@angular/core';
import { fadeIn } from 'src/app/router.animation';
import { DepartmentsService } from 'src/app/module/share/service/deparments/departments.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [fadeIn()]
})
export class DashboardComponent implements OnInit {
  totalUser = 0;
  totalOrder = 0;
  newOrder = 0;
  totalDepartment = 0;
  isVerify = false;
  constructor(
    private dep: DepartmentsService
  ) {
    this.isVerify = false;
  }

  ngOnInit() {
    this.dep.getDepartments().subscribe(res => {
      this.totalDepartment = res.length;
    })
  }
  getTotalUser(number) {
    this.totalUser = number;
  }
  getNewOrders(number) {
    this.newOrder = number;
  }
  getTotalOrders(number) {
    this.totalOrder = number;
  }
}
