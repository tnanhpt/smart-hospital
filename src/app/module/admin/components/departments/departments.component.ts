import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { DepartmentModel, DepartmentIDModel, RolesModel } from 'src/app/module/core/auth/model/core.model';
import { map, take, filter } from 'rxjs/operators';
import { AuthService } from 'src/app/module/core/auth/service/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { CreateDepartmentComponent } from './create-department/create-department.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { EditDepartmentsComponent } from './edit-departments/edit-departments.component';
import { DeleteDialogComponent } from 'src/app/module/share/dialog/delete-dialog/delete-dialog.component';
import { MatSort } from '@angular/material/sort';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.scss']
})
export class DepartmentsComponent implements OnInit {
  derpartments$: Observable<DepartmentIDModel[]>;
  derpartments: DepartmentModel[];
  derpartmentSingle: DepartmentModel[];
  departmentCol: AngularFirestoreCollection<DepartmentModel>;
  departmentDoc: AngularFirestoreDocument<DepartmentModel>;
  departmentSingle: Observable<DepartmentModel>;
  name: string;
  displayedColumns: string[] = ['name', 'orderNumberCurrent', 'action'];
  dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private afs: AngularFirestore,
    public auth: AuthService,
    public dialog: MatDialog,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.departmentCol = this.afs.collection('departments');
    this.departmentCol.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as DepartmentModel;
          const id = a.payload.doc.id;
          return { id, data };
        });
      })).subscribe(res => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    this.departmentCol.valueChanges().subscribe(
      res => {
        this.derpartments = res;
      }
    );
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  addDepartment() {
    this.auth.getDoctors().subscribe(res => {
      if (res) {
        // console.log(res);

        const dialogRef = this.dialog.open(CreateDepartmentComponent, {
          width: '350px',
          data: { doctors: res }
        });
        dialogRef.afterClosed().pipe(take(1)).subscribe(result => {
          if (result) {
            // console.log(result);
            
            this.afs.collection('departments').add({ 'name': result.label, 'orderNumberCurrent': 0, doctors: result.doctors});
            this.toastr.success('Thêm thành công!');
          }
        });
      }
    });
  }
  getDepartment(id) {
    this.departmentDoc = this.afs.doc('departments/' + id);
    this.departmentSingle = this.departmentDoc.valueChanges();

  }
  deleteDepartment(id) {
    this.departmentDoc = this.afs.doc('departments/' + id);
    this.departmentDoc.valueChanges().pipe(take(1)).subscribe(res => {
      const dialogRef = this.dialog.open(DeleteDialogComponent, {
        width: '350px',
        data: { label: res.name, title: 'Bạn có muốn xóa  ' + res.name + ' không?' }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {

          this.afs.doc('departments/' + id).delete();
          this.toastr.success('Đã xóa!');
        }
      });
    });

  }
  editDepartment(id) {
    this.departmentDoc = this.afs.doc('departments/' + id);
    this.departmentDoc.valueChanges().pipe(take(1)).subscribe(res => {
      const dialogRef = this.dialog.open(EditDepartmentsComponent, {
        width: '300px',
        data: { label: res.name, number: res.orderNumberCurrent }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          let data: DepartmentModel;
          data = {
            ...data,
            name: result.department,
            orderNumberCurrent: result.number
          }
          this.departmentDoc.update(data);
          this.toastr.success('Sửa thành công!');
        }
      });
    });
  }
}