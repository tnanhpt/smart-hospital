import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserModel } from 'src/app/module/core/auth/model/core.model';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-create-department',
  templateUrl: './create-department.component.html',
  styleUrls: ['./create-department.component.scss']
})
export class CreateDepartmentComponent implements OnInit {
  doctorValue = new FormControl();
  label = new FormControl();
  doctors: UserModel[];
  departmentForm = new FormControl({

  })
  constructor(
    public dialogRef: MatDialogRef<CreateDepartmentComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onSubmit() {
    // this.doctorValue.value

    const data = {
      label: this.label.value,
      doctors: this.doctorValue.value
    }
    return data;
  }
  ngOnInit() {
    if (this.data.doctors) {
      this.doctors = this.data.doctors.map(a => {
        let doctorModified: UserModel = {
          displayName: a.displayName,
          photoURL: a.photoURL,
          uid: a.uid
        }
        return doctorModified;
      });
    }
  }

}
