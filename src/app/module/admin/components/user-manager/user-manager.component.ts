import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/module/core/auth/service/auth.service';
import { Observable } from 'rxjs';
import { UserModel, RolesModel } from 'src/app/module/core/auth/model/core.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { take, map } from 'rxjs/operators';
import { DeleteDialogComponent } from 'src/app/module/share/dialog/delete-dialog/delete-dialog.component';
import * as moment from 'moment';
import { EditUserComponent } from './edit-user/edit-user.component';

@Component({
  selector: 'app-user-manager',
  templateUrl: './user-manager.component.html',
  styleUrls: ['./user-manager.component.scss']
})
export class UserManagerComponent implements OnInit {
  users$: Observable<UserModel>;
  users: UserModel[];
  userSingle: UserModel[];
  usersCol: AngularFirestoreCollection<UserModel>;
  usersDoc: AngularFirestoreDocument<UserModel>;
  dataSource;
  displayedColumns: string[] = ['photoURL', 'displayName', 'address', 'birthday', 'gender', 'roles', 'phoneNumber', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(
    private afs: AngularFirestore,
    public auth: AuthService,
    public dialog: MatDialog,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.usersCol = this.afs.collection('users');
    this.usersCol.valueChanges().pipe(map((data => data.map(a => {
      let dataS: UserModel;
      if (a.birthday !== null) {
        dataS = {
          ...a,
          birthday: moment.unix(a.birthday.seconds)
        }
        return dataS;
      }
      else {
        return a;
      }
    }
    )))).subscribe(res => {
      if (res) {
        this.dataSource = new MatTableDataSource<UserModel>(res);
        this.dataSource.paginator = this.paginator;
      }
    })
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  deleteUser(id) {
    this.usersDoc = this.afs.doc('users/' + id);
    this.usersDoc.valueChanges().pipe(take(1)).subscribe(res => {
      const dialogRef = this.dialog.open(DeleteDialogComponent, {
        width: '400px',
        data: { title: 'Bạn có muốn xóa đặt lịch của ' + res.displayName + ' không?' }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.afs.doc('users/' + id).delete();
          this.toastr.success('Đã xóa!');
        }
      });
    });
  }
  editUser(id) {
    this.usersDoc = this.afs.doc('users/' + id);
    this.usersDoc.valueChanges().pipe(take(1)).subscribe(res => {
      const dialogRef = this.dialog.open(EditUserComponent, {
        maxWidth: '800px',
        height: 'auto',
        data: { user: res }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          let data: UserModel;
          data = {
            ...data,
            displayName: result.displayName || null,
            roles: result.roles || RolesModel.member,
            phoneNumber: result.phoneNumber || null,
            address: result.address || null,
            gender: result.gender || null,
            birthday: result.birthday || null
          }
          this.usersDoc.update(data);
          this.toastr.success('Sửa thành công!');
        }
      });
    });
  }
  addUser() {
    const dialogRef = this.dialog.open(EditUserComponent, {
      maxWidth: '800px',
      height: 'auto',
      data: {isCreate: true}
    });
    dialogRef.afterClosed().pipe(take(1)).subscribe(result => {
      if (result) {
        let data: UserModel;
        data = {
          ...data,
          displayName: result.displayName || null,
          password: result.password,
          email: result.email,
          roles: RolesModel.member,
          phoneNumber: result.phoneNumber || null,
          address: result.address || null,
          gender: result.gender || null,
          birthday: result.birthday || null
        }
        this.toastr.success('Thêm thành công!');
        return this.auth.addUser(data);
        
      }
    });
  }
}

