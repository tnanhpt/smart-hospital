import { Component, OnInit, Inject, SimpleChanges, OnChanges } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserModel } from 'src/app/module/core/auth/model/core.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import * as moment from 'moment';
@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  maxDate = new Date();
  user: UserModel;
  updateInfoForm = new FormGroup({
    displayName: new FormControl('', [Validators.required]),
    phoneNumber: new FormControl('', [Validators.required]),
    email: new FormControl({ value: '', disabled: !this.data.isCreate }, [Validators.required,Validators.email]),
    birthday: new FormControl('', [Validators.required]),
    address: new FormControl('', [Validators.required]),
    password: new FormControl(''),
    gender: new FormControl('', [Validators.required]),
    roles: new FormControl('', [Validators.required])
  })
  constructor(
    public dialogRef: MatDialogRef<EditUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private dateAdapter: DateAdapter<Date>, ) {
    this.dateAdapter.setLocale('vi');
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.data.user.birthday !== null) {
      const day = moment.unix(this.data.user.birthday.seconds);
      const dayD = day.format();
      const birthday = moment(dayD).format("YYYY-MM-DD");
      this.updateInfoForm.setValue({
        displayName: this.data.user.displayName,
        address: this.data.user.address,
        phoneNumber: this.data.user.phoneNumber,
        email: this.data.user.email,
        birthday: birthday,
        gender: this.data.user.gender,
        roles: this.data.user.roles,
        password: ''
      })
    } else {
      this.updateInfoForm.setValue({
        displayName: this.data.user.displayName,
        address: this.data.user.address,
        phoneNumber: this.data.user.phoneNumber,
        email: this.data.user.email,
        birthday: this.data.user.birthday,
        gender: this.data.user.gender,
        roles: this.data.user.roles,
        password: ''
      })
    }
  }
  getErrorMessage() {
    
    return this.updateInfoForm.value.email.hasError('required') ? 'Dịa chỉ email không được để trống' : this.updateInfoForm.value.email.hasError('email') ? 'Email không đúng định dạng' :
            '';
  }
}
