import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AuthService } from 'src/app/module/core/auth/service/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { OrderIdModel, OrderModel, StatusOrder } from 'src/app/module/core/auth/model/core.model';
import { map, take } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import * as moment from 'moment';
import { DeleteDialogComponent } from 'src/app/module/share/dialog/delete-dialog/delete-dialog.component';
import { ToastrService } from 'ngx-toastr';
import { ConfirmDialogComponent } from 'src/app/module/share/dialog/confirm-dialog/confirm-dialog.component';
import { EditOrderComponent } from './edit-order/edit-order.component';
import { OrderService } from 'src/app/module/share/service/order/order.service';
import { fadeIn } from 'src/app/router.animation';
@Component({
  selector: 'app-orders-manager',
  templateUrl: './orders-manager.component.html',
  styleUrls: ['./orders-manager.component.scss']
})
export class OrdersManagerComponent implements OnInit {
  orders$: Observable<OrderIdModel>;
  orders: OrderModel[];
  ordersId: any;
  orderSingle: OrderModel[];
  orderCol: AngularFirestoreCollection<OrderModel>;
  orderDoc: AngularFirestoreDocument<OrderModel>;
  dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['displayName', 'address', 'date', 'birthday', 'gender', 'dateOrder', 'orderNumber', 'department', 'healthStatus', 'phoneNumber', 'status', 'action'];
  constructor(
    private afs: AngularFirestore,
    public auth: AuthService,
    public dialog: MatDialog,
    private toastr: ToastrService,
    private os: OrderService,
  ) { }

  ngOnInit() {
    this.orderCol = this.afs.collection('orders', ref =>
      ref.orderBy('dateOrder', 'desc'));
    this.orderCol.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const dataBefore = a.payload.doc.data() as OrderModel;
          console.log(a);
          
          const id = a.payload.doc.id;
          let data: OrderModel;
          if (dataBefore.birthday !== null && dataBefore.date !== null && dataBefore.dateOrder !== null) {
            data = {
              ...dataBefore,
              birthday:  moment.unix(dataBefore.birthday.seconds),
              date: moment.unix(dataBefore.date.seconds),
              dateOrder: moment.unix(dataBefore.dateOrder.seconds),
            }
            return { id, data };
          } else {
            return { id, dataBefore };
          }
        });
      })).subscribe(res => {
        if (res) {
          console.log(JSON.stringify(res));

          this.dataSource = new MatTableDataSource(res);
          this.dataSource.paginator = this.paginator;
        }
      });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  deleteOrder(id) {
    this.orderDoc = this.afs.doc('orders/' + id);
    this.orderDoc.valueChanges().pipe(take(1)).subscribe(res => {
      const dialogRef = this.dialog.open(DeleteDialogComponent, {
        width: '400px',
        data: { title: 'Bạn có muốn xóa đặt lịch của ' + res.displayName + ' không?' }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {

          this.afs.doc('orders/' + id).delete();
          this.toastr.success('Đã xóa!');
        }
      });
    });
  }
  changeStatus(type, id) {
    this.orderDoc = this.afs.doc('orders/' + id);
    switch (type) {
      case 'confirmed': {
        this.orderDoc.valueChanges().pipe(take(1)).subscribe(res => {
          const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            width: 'auto',
            data: { title: 'Bạn có muốn xác nhận đặt lịch cho ' + res.displayName }
          });
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              let data: OrderModel;
              data = {
                ...data,
                status: StatusOrder.confirmed
              }
              this.orderDoc.update(data);
              this.toastr.success('Thành công!');
            }
          });
        });
        break;
      }
      case 'completed': {
        this.orderDoc.valueChanges().pipe(take(1)).subscribe(res => {
          const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            width: '400px',
            data: { title: 'Bạn có muốn hoàn thành lịch khám cho ' + res.displayName }
          });
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              let data: OrderModel;
              data = {
                ...data,
                status: StatusOrder.completed
              }
              this.orderDoc.update(data);
              this.toastr.success('Thành công!');
            }
          });
        });
        break;
      }
      case 'canceled': {
        this.orderDoc.valueChanges().pipe(take(1)).subscribe(res => {
          const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            width: '400px',
            data: { title: 'Bạn có muốn hủy đặt lịch cho ' + res.displayName }
          });
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              let data: OrderModel;
              data = {
                ...data,
                status: StatusOrder.canceled
              }
              this.orderDoc.update(data);
              this.toastr.success('Hủy thành công!');
            }
          });
        });
        break;
      }
      default: {
        break;
      }
    }
  }
  editOrder(id) {
    this.orderDoc = this.afs.doc('orders/' + id);
    this.orderDoc.valueChanges().pipe(take(1), map(
      data => {
        if (data.birthday !== null && data.date !== null && data.dateOrder !== null) {
          let orderM: OrderModel;
          orderM = {
            ...data,
            birthday: moment.unix(data.birthday.seconds),
            date: moment.unix(data.date.seconds),
            dateOrder: moment.unix(data.dateOrder.seconds)
          }
          return orderM;
        } else {
          return data;
        }
      }
    )).subscribe(res => {
      const dialogRef = this.dialog.open(EditOrderComponent, {
        maxWidth: '800px',
        height: 'auto',
        data: { order: res }
      });

      dialogRef.afterClosed().pipe(take(1)).subscribe(result => {
        if (result) {
          let data: OrderModel;
          data = {
            ...data,
            displayName: result.displayName || null,
            healthStatus: result.healthStatus,
            email: result.email,
            date: result.date,
            department: result.departmentName,
            status: result.status,
            phoneNumber: result.phoneNumber || null,
            address: result.address || null,
            gender: result.gender || null,
            birthday: result.birthday || null
          }
          this.orderDoc.update(data);
          this.toastr.success('Sửa thành công!');

        }

      });
    });
  }
  addOrder() {
      const dialogRef = this.dialog.open(EditOrderComponent, {
        maxWidth: '800px',
        height: 'auto',
        data: { isCreate: true}
      });
      dialogRef.afterClosed().pipe(take(1)).subscribe(result => {
        if (result) {
          const ordersCollection: AngularFirestoreCollection<OrderModel> = this.afs.collection<any>('orders', ref =>
          ref.where('date', '==', result.date)
        );
        ordersCollection.snapshotChanges().pipe(
          map(actions => {
            return actions.map(a => {
              const data = a.payload.doc.data() as OrderModel;
              const id = a.payload.doc.id;
              return { id, data };
            });
          })).pipe(take(1)).subscribe(order => {
            const numberOrder = order.length + 1 
            this.os.addOrder(result, null, numberOrder);
          })
          this.toastr.success('Đặt lịch thành công');
        }
      });
  }
}

