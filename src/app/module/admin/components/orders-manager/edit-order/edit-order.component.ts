import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DateAdapter } from '@angular/material/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { DepartmentIDModel } from 'src/app/module/core/auth/model/core.model';
import { Observable } from 'rxjs';
import { DepartmentsService } from 'src/app/module/share/service/deparments/departments.service';
@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.scss']
})
export class EditOrderComponent implements OnInit {
  checkoutForm = new FormGroup({
    displayName: new FormControl(''),
    // age: new FormControl(''),
    address: new FormControl(''),
    phoneNumber: new FormControl(''),
    email: new FormControl(''),
    birthday: new FormControl(''),
    gender: new FormControl(''),
    healthStatus: new FormControl(''),
    date: new FormControl(''),
    departmentName: new FormControl(''),
    status: new FormControl('')
  });
  minDate = new Date();
  maxDate = new Date();
  derpartments$: Observable<DepartmentIDModel[]>;
  constructor(
    public departmentS: DepartmentsService,
    public dialogRef: MatDialogRef<EditOrderComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private dateAdapter: DateAdapter<Date>) {
    this.dateAdapter.setLocale('vi');
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.derpartments$ = this.departmentS.getDepartments();
    if (this.data.order) {
      if (this.data.order.birthday !== null && this.data.order.date !== null && this.data.order.dateOrder !== null) {
        const birthday = moment(this.data.order.birthday.format()).format("YYYY-MM-DD");
        const date = moment(this.data.order.date.format()).format("YYYY-MM-DD");

        this.checkoutForm.setValue({
          displayName: this.data.order.displayName,
          address: this.data.order.address,
          phoneNumber: this.data.order.phoneNumber,
          email: this.data.order.email,
          birthday: birthday,
          gender: this.data.order.gender,
          healthStatus: this.data.order.healthStatus || null,
          departmentName: this.data.order.department,
          date: date,
          status: this.data.order.status
        })
      } else {
        this.checkoutForm.setValue({
          displayName: this.data.order.displayName,
          address: this.data.order.address,
          phoneNumber: this.data.order.phoneNumber,
          email: this.data.order.email,
          birthday: this.data.order.birthday,
          gender: this.data.order.gender,
          healthStatus: this.data.order.healthStatus,
          departmentName: this.data.order.department,
          date: this.data.order.date,
          status: this.data.order.status
        })
      }
    }
  }s

}
