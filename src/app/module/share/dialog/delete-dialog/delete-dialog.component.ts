import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EditDepartmentsComponent } from 'src/app/module/admin/components/departments/edit-departments/edit-departments.component';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss']
})
export class DeleteDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { }



  ngOnInit() {
    console.log(this.data);
    
  }
  onCloseDialog(isSubmit): void {
    this.dialogRef.close(isSubmit);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
