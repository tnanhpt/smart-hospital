import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from 'src/app/module/core/auth/service/auth.service';
import { OrderModel, OrderIdModel, DepartmentModel, StatusOrder } from 'src/app/module/core/auth/model/core.model';
import { map, last } from 'rxjs/operators';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  odersCollection: AngularFirestoreCollection<OrderModel>;
  constructor(private afs: AngularFirestore, private auth: AuthService) { }
  getTimeServer() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }

  getOrder() {
    this.odersCollection = this.afs.collection<OrderModel>('orders', ref =>
      ref.orderBy('dateOrder', 'desc')
    );
    const order = this.odersCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as OrderModel;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
    return order;
  }
  getOrderByUID(uid) {
    this.odersCollection = this.afs.collection<OrderModel>('orders', ref =>
      ref.orderBy('dateOrder', 'desc').where('uid', '==', uid)
    );
    const order = this.odersCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as OrderModel;
        const id = a.payload.doc.id;
        if(data.date !== null) {
          const dataX = {
            ...data,
            date: moment.unix(data.date.seconds)
          }
          return { id, ...dataX };
        }
       
      
      }))
    );
    return order;
  }
  getLastOrderDepartment(department) {
    this.odersCollection = this.afs.collection<any>('orders', ref =>
      ref.orderBy('dateOrder', 'desc')
        .where('department', '==', department)
        .limit(1),
    );
    const lastOrder = this.odersCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as any;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
    return lastOrder;
  }
  // getLastOrder() {
  //   const odersCollection: AngularFirestoreCollection<OrderModel> = this.afs.collection<any>('orders', ref 
  //     ref.orderBy('dateOrder', 'desc')
  //       .limit(1),
  //   );
  //   odersCollection.valueChanges().subscribe(data => {
  //     // console.log(data[0].department);
  //     const departmentCollection: AngularFirestoreCollection<DepartmentModel> = this.afs.collection<any>('departments', ref =>
  //       ref.where('name', '==', data[0].department)
  //         .limit(1),
  //     );
  //     departmentCollection.valueChanges().subscribe(department => {
  //       console.log(department[0].orderNumberCurrent + ' depart');
  //       console.log(data[0].department);
  //     })

  //   });

  // }
  addOrder(data, uid, orderNumberLast) {
    this.afs.collection<OrderModel>('orders').add(
      {
        'dateOrder': this.getTimeServer(),
        'date': data.date,
        'orderNumber': orderNumberLast,
        'healthStatus': data.healthStatus,
        'department': data.departmentName,
        'displayName': data.displayName,
        'uid': uid || null,
        'email': data.email,
        'birthday': data.birthday,
        'phoneNumber': data.phoneNumber,
        'address': data.address,
        'gender': data.gender,
        'status': StatusOrder.pending
      }
    )
      .then((success) => {
        const suceessDoc = this.afs.doc('orders/' + success.id);
        suceessDoc.valueChanges().subscribe(res => {
          return res;
        })
      }
      )
      .catch(error => console.log(error));
  }
}