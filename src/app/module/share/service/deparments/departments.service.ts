import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { DepartmentModel, DepartmentIDModel } from 'src/app/module/core/auth/model/core.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DepartmentsService {
  derpartments$: Observable<DepartmentIDModel[]>;
  departmentDoc: AngularFirestoreDocument<DepartmentModel>;
  departmentSingle: Observable<DepartmentModel>;
  name: string;
  constructor(private afs: AngularFirestore) {
  }
  getDepartments() {
    const departmentCol: AngularFirestoreCollection<DepartmentModel> = this.afs.collection('departments');
    return departmentCol.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as DepartmentModel;
          const id = a.payload.doc.id;
          return { id, data };
        });
      }));
  }
}
