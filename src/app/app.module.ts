import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { CoreModule } from './module/core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatChipsModule } from '@angular/material/chips';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatListModule } from '@angular/material/list';
import { MatTabsModule } from '@angular/material/tabs';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTableModule } from '@angular/material/table';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatStepperModule } from '@angular/material/stepper';

// Component
import { SignInComponent } from './module/auth/components/sign-in/sign-in.component';
import { ReservationsComponent } from './module/reservations/components/reservations.component';
import { UserProfileComponent } from './module/auth/components/user-profile/user-profile.component';
import { ContainerComponent } from './module/container/components/container.component';
import { AuthRoutingModule } from './module/auth/auth-routing.module';
import { DepartmentsComponent } from './module/admin/components/departments/departments.component';
import { DepartmentsService } from './module/share/service/deparments/departments.service';
import { OrderService } from './module/share/service/order/order.service';
import { PersonalInformationComponent } from './module/auth/components/user-profile/personal-information/personal-information.component';
import { MatNativeDateModule } from '@angular/material/core';
import { OrderHistoryComponent } from './module/auth/components/user-profile/order-history/order-history.component';
import { DropZoneDirective } from './module/share/directive/drop-zone.directive';
import { ToastrModule } from 'ngx-toastr';
import { DepartmentsInfomationComponent } from './module/infomation/departments-infomation/departments-infomation.component';
import { AdminPageComponent } from './module/admin/admin-page-container/admin-page.component';
import { AdminRouterModule } from './module/admin/routers/admin-router.module';
import { CreateDepartmentComponent } from './module/admin/components/departments/create-department/create-department.component';
import { MatDialogModule } from '@angular/material/dialog';
import { EditDepartmentsComponent } from './module/admin/components/departments/edit-departments/edit-departments.component';
import { DeleteDialogComponent } from './module/share/dialog/delete-dialog/delete-dialog.component';
import { OrdersManagerComponent } from './module/admin/components/orders-manager/orders-manager.component';
import { ConfirmDialogComponent } from './module/share/dialog/confirm-dialog/confirm-dialog.component';
import { UserManagerComponent } from './module/admin/components/user-manager/user-manager.component';
import { EditUserComponent } from './module/admin/components/user-manager/edit-user/edit-user.component';
import { EditOrderComponent } from './module/admin/components/orders-manager/edit-order/edit-order.component';
import { DashboardComponent } from './module/admin/components/dashboard/dashboard.component';
import { OrderNewComponent } from './module/admin/components/dashboard/order-new/order-new.component';
import { UserNewComponent } from './module/admin/components/dashboard/user-new/user-new.component';
import { AddOrderComponent } from './module/admin/components/orders-manager/add-order/add-order.component';
import { HomeComponent } from './module/home/index/home/home.component';
import { ToolbarComponent } from './share/toolbar/toolbar.component';
import { WindowService } from './module/share/service/window/window.service';


@NgModule({
    declarations: [
        AppComponent,
        SignInComponent,
        ReservationsComponent,
        UserProfileComponent,
        ContainerComponent,
        AdminPageComponent,
        DepartmentsComponent,
        PersonalInformationComponent,
        OrderHistoryComponent,
        DropZoneDirective,
        DepartmentsInfomationComponent,
        CreateDepartmentComponent,
        EditDepartmentsComponent,
        DeleteDialogComponent,
        OrdersManagerComponent,
        ConfirmDialogComponent,
        UserManagerComponent,
        EditUserComponent,
        EditOrderComponent,
        DashboardComponent,
        OrderNewComponent,
        UserNewComponent,
        AddOrderComponent,
        HomeComponent,
        ToolbarComponent
    ],
    imports: [
        AuthRoutingModule,
        AdminRouterModule,
        BrowserModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFireDatabaseModule,
        AngularFirestoreModule,
        AngularFireStorageModule,
        CoreModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        FlexLayoutModule,
        // Material Module
        MatCardModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatIconModule,
        MatChipsModule,
        MatToolbarModule,
        MatSidenavModule,
        MatMenuModule,
        MatPaginatorModule,
        MatCheckboxModule,
        MatListModule,
        MatTabsModule,
        MatRadioModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatTableModule,
        MatProgressBarModule,
        ToastrModule.forRoot(),
        NgxMatSelectSearchModule
    ],
    entryComponents: [
        CreateDepartmentComponent,
        EditDepartmentsComponent,
        DeleteDialogComponent,
        ConfirmDialogComponent,
        EditUserComponent,
        EditOrderComponent
    ],
    providers: [OrderService, DepartmentsService, WindowService],
    bootstrap: [AppComponent]
})
export class AppModule { }
