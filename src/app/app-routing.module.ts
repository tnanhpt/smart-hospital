import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './module/core/auth/auth.guard';
import { ContainerComponent } from './module/container/components/container.component';
import { ReservationsComponent } from './module/reservations/components/reservations.component';
import { UserProfileComponent } from './module/auth/components/user-profile/user-profile.component';
import { PersonalInformationComponent } from './module/auth/components/user-profile/personal-information/personal-information.component';
import { OrderHistoryComponent } from './module/auth/components/user-profile/order-history/order-history.component';
import { DepartmentsInfomationComponent } from './module/infomation/departments-infomation/departments-infomation.component';
import { HomeComponent } from './module/home/index/home/home.component';

// import { CommonModule } from '@angular/common';
const routes: Routes = [
  {
    path: '',
    redirectTo: '/app/dat-lich',
    pathMatch: 'full'
  },
  {
    path: 'app',
    canActivate: [AuthGuard],
    component: ContainerComponent,
    children: [
      {
        path: 'tra-cuu-khoa', component: DepartmentsInfomationComponent
      },
      { path: 'dat-lich', component: ReservationsComponent },
      {
        path: 'tai-khoan',
        component: UserProfileComponent,
        children: [
          { path: '', component: PersonalInformationComponent },
          { path: 'thong-tin', component: PersonalInformationComponent },
          { path: 'lich-su', component: OrderHistoryComponent }
        ]
      },
      {
        path: 'bac-si',
        component: DepartmentsInfomationComponent
      }
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
