export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCEpuOJ23GfA3FgIzKD6Y4fpVBxHiyVYXI',
    authDomain: 'smart-hospital-84904.firebaseapp.com',
    databaseURL: 'https://smart-hospital-84904.firebaseio.com',
    projectId: 'smart-hospital-84904',
    storageBucket: 'smart-hospital-84904.appspot.com',
    messagingSenderId: '845004544409'
  }
};
