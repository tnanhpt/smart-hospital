// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCEpuOJ23GfA3FgIzKD6Y4fpVBxHiyVYXI',
    authDomain: 'smart-hospital-84904.firebaseapp.com',
    databaseURL: 'https://smart-hospital-84904.firebaseio.com',
    projectId: 'smart-hospital-84904',
    storageBucket: 'smart-hospital-84904.appspot.com',
    messagingSenderId: '845004544409'
  }
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
